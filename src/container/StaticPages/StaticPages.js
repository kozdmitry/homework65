import React, {useEffect, useState} from 'react';
import axiosPages from "../../axiosPages";
import {Paper, Typography} from "@material-ui/core";
import "./StaticPages.css";

const StaticPages = (props) => {
    const name = props.match.params.name;
    const [page, setPage] = useState([]);

    useEffect(() => {
        axiosPages.get(name + '.json').then(result => {
            setPage(result.data)
        }, e => {
            console.log(e);
        });
    }, [name]);

    return (
        <>
            <Paper className="staticPages">
                <Typography/>
                <h3>{page.title}</h3>
                <p>{page.content}</p>
            </Paper>

        </>
    );
};
export default StaticPages;